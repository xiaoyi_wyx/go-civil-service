module test-build

go 1.20

require golang.org/x/text v0.12.0

require (
	github.com/fumiama/go-docx v0.0.0-20230710063609-9f3162a90fb6 // indirect
	github.com/fumiama/imgsz v0.0.2 // indirect
)
