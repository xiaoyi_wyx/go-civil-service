package main

import (
	"fmt"
	"os"
)

func CreateFile(name string) {
	file, err := os.Create("log/" + name + ".txt")
	if err != nil {
		fmt.Println(err)
	}
	file.Write([]byte("byte"))
}
