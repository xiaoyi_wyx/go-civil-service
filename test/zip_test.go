package test

import (
	"archive/zip"
	"bytes"
	"fmt"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io"
	"log"
	"os"
	"path/filepath"
	"testing"
)

func TestZipOpen(t *testing.T) {
	// 打开 ZIP 文件
	r, err := zip.OpenReader("../exam-pre/燎原2023年1-2月时事政治热点等.zip")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	var decodeName string
	// 遍历 ZIP 中的文件
	for _, f := range r.File {
		if f.Flags == 0 {
			i := bytes.NewReader([]byte(f.Name))
			decoder := transform.NewReader(i, simplifiedchinese.GB18030.NewDecoder())
			content, _ := io.ReadAll(decoder)
			decodeName = string(content)
		} else {
			decodeName = f.Name
		}
		fmt.Printf("Extracting %s\n", decodeName)

		func() {
			// 打开文件
			rc, err := f.Open()
			if err != nil {
				log.Fatal(err)
			}
			defer rc.Close()

			// 创建目标文件
			path := filepath.Join("../exam-pre", decodeName)
			if f.FileInfo().IsDir() {
				os.MkdirAll(path, f.Mode())
			} else {
				os.MkdirAll(filepath.Dir(path), f.Mode())
				f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
				if err != nil {
					log.Fatal(err)
				}
				defer f.Close()

				// 复制文件内容
				_, err = io.Copy(f, rc)
				if err != nil {
					log.Fatal(err)
				}
			}
		}()
	}
}
