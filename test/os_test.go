package test

import (
	"fmt"
	"os"
	"testing"
)

func TestCreateFile(t *testing.T) {
	file, err := os.Create("../log" + "/demo1.txt")
	if err != nil {
		t.Error(err)
		return
	}
	defer file.Close()

	_, err = file.Write([]byte("demo test"))
	if err != nil {
		t.Error(err)
		return
	}
}

func TestReadFile(t *testing.T) {
	file, err := os.Open("../log" + "/demo1.txt")
	if err != nil {
		t.Error(err)
		return
	}
	defer file.Close()

	bytes := make([]byte, 1024)
	n, err := file.Read(bytes)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(bytes[:n]))
}
