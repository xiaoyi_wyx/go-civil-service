package test

import (
	"fmt"
	"github.com/fumiama/go-docx"
	"os"
	_ "strings"
	"testing"
)

func TestReadWord(t *testing.T) {
	readFile, err := os.Open("../exam-pre/2023.8月言语理解/言语理解与表达-逻辑填空-实词辨析4（3.11）.docx")
	if err != nil {
		panic(err)
	}
	defer readFile.Close()

	fileInfo, err := readFile.Stat()
	if err != nil {
		panic(err)
	}
	size := fileInfo.Size()
	doc, err := docx.Parse(readFile, size)
	if err != nil {
		panic(err)
	}
	for _, it := range doc.Document.Body.Items {
		switch it.(type) {
		case *docx.Paragraph: // printable
			sprint := fmt.Sprintln(it)
			fmt.Println(sprint)
		}
	}

}
