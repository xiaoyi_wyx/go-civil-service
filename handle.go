package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func handleDemo(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("./templates/index.html")
	if err != nil {
		fmt.Println(err)
		w.Write([]byte(err.Error()))
	}
	t.Execute(w, map[string]interface{}{})
}
