package main

import (
	"net/http"
)

func Register() *http.ServeMux {
	r := http.NewServeMux()
	r.HandleFunc("/", handleDemo)
	return r
}
